/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {
    
    public static void main(String[] args) {
        Conjunto<Integer> c1=new Conjunto(6);
        Conjunto<Integer> c2=new Conjunto(5);
        Conjunto<Integer> c3=new Conjunto(8);
        Conjunto<Integer> c4=new Conjunto(4);
        
        
        //probar todos los métodos públicos(imprimir)
        //Recomendación: Realicen método para crear cada una de las pruebas.
        try{
            
            c1.adicionarElemento(15);
            c1.adicionarElemento(20);
            c1.adicionarElemento(5);
            c1.adicionarElemento(9);
            c1.adicionarElemento(10);
            c1.adicionarElemento(8);
            c2.adicionarElemento(1);
            c2.adicionarElemento(2);
            c2.adicionarElemento(14);
            c2.adicionarElemento(6);
            c2.adicionarElemento(10);
            c3.adicionarElemento(16);
            c3.adicionarElemento(18);
            c3.adicionarElemento(9);
            c3.adicionarElemento(4);
            c3.adicionarElemento(7);
            c3.adicionarElemento(3);
            c3.adicionarElemento(15);
            c3.adicionarElemento(12);
            c4.adicionarElemento(1);
            c4.adicionarElemento(2);
            c4.adicionarElemento(3);
            c4.adicionarElemento(4);
            
            System.out.println(c2.getDiferencia(c4));
            
            c1.ordenar();
            System.out.println("Este es el conjunto 1 ordenado por inserción");
            System.out.println(c1.toString());
            
            /*
            c2.ordenarBurbuja();
            System.out.println("Este es el conjunto 2 ordenado por burbuja");
            System.out.println(c2.toString());
            
            
            c1.concatenar(c3);
            System.out.println("Acá concatenamps c1 y c3 normal");
            System.out.println(c1.toString());
           
            
            c2.concatenarRestrictivo(c4);
            System.out.println("Acá concatenamos restrictivo a c2 y c4");
            System.out.println(c2.toString());
           
            
           
            
            c1.remover(9);
            System.out.println("Este es el método remover para un solo elemento");
            System.out.println(c1.toString());
            
            
            System.out.println("Resultado final de los conjuntos");
            System.out.println(c1.toString());
            System.out.println(c2.toString());
            System.out.println(c3.toString());
            System.out.println(c4.toString());
            */
           //C3 y C4 fueron eliminador por ser el conjunto nuevo en los metodos Concatenar
            
        }
        catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    
}
