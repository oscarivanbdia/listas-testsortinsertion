/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Util.Lista;
/**
 *
 * @author RYZEN
 */
public class TestListaS {

    public static void main(String[] args) {
        /* Lista<Integer> lista1 = new Lista();
        for(int i=20000; i>=1; i--){
            lista1.insertarFinal(i);
        }
        long horaI1 = System.nanoTime();
        lista1.ordenarInsercion_Por_Infos();
        long horaF1 = System.nanoTime()-horaI1;
        //System.out.println( lista1.toString());
        System.out.println("Método por Infos para 20000 elementos");
        System.out.println(horaF1 + " Nanosegundos");
        
        Lista<Integer> lista2 = new Lista();
        for(int i=20000; i>=1; i--){
            lista2.insertarFinal(i);
        }
        long horaI2 = System.nanoTime();
        lista2.ordenarInsercion_Por_Nodos();
        long horaF2 = System.nanoTime()-horaI2;
        //System.out.println( lista2.toString());
        System.out.println("Método por Nodos para 20000 elementos");
        System.out.println(horaF2 + " Nanosegundos");
        
        Lista<Integer> lista3 = new Lista();
        for(int i=200000; i>=1; i--){
            lista3.insertarFinal(i);
        }
        long horaI3 = System.nanoTime();
        lista3.ordenarInsercion_Por_Infos();
        long horaF3 = System.nanoTime()-horaI3;
        //System.out.println( lista3.toString());
        System.out.println("Método por Infos para 200000 elementos");
        System.out.println(horaF3 + " Nanosegundos");
        
        Lista<Integer> lista4 = new Lista();
        for(int i=200000; i>=1; i--){
            lista4.insertarFinal(i);
        }
        long horaI4 = System.nanoTime();
        lista4.ordenarInsercion_Por_Nodos();
        long horaF4 = System.nanoTime()-horaI4;
        //System.out.println( lista4.toString());
        System.out.println("Método por Nodos para 200000 elementos");
        System.out.println(horaF4 + " Nanosegundos");
        
        Lista<Integer> lista5 = new Lista();
        for(int i=5000000; i>=1; i--){
            lista5.insertarFinal(i);
        }
        long horaI5 = System.nanoTime();
        lista5.ordenarInsercion_Por_Infos();
        long horaF5 = System.nanoTime()-horaI5;
        //System.out.println( lista5.toString());
        System.out.println("Método por Infos para 5000000 elementos");
        System.out.println(horaF5 + " Nanosegundos");
        */
        Lista<Integer> lista6 = new Lista();
        for(int i=5000000; i>=1; i--){
            lista6.insertarFinal(i);
        }
        long horaI6 = System.nanoTime();
        lista6.ordenarInsercion_Por_Nodos();
        long horaF6 = System.nanoTime()-horaI6;
        //System.out.println( lista6.toString());
        System.out.println("Método por Nodos para 5000000 elementos");
        System.out.println(horaF6 + " Nanosegundos");
        
        /*
        Lista<Integer> lista7 = new Lista();
        for(int i=20000; i>=1; i--){
            int numeroAleatorio = (int) (Math.random()*20000+1);
            lista7.insertarFinal(numeroAleatorio);
        }
        long horaI7 = System.nanoTime();
        lista7.ordenarInsercion_Por_Infos();
        long horaF7 = System.nanoTime()-horaI7;
        //System.out.println( lista7.toString());
        System.out.println("Método por Infos para 20000 elementos aleatorios");
        System.out.println(horaF7 + " Nanosegundos");
        
        Lista<Integer> lista8 = new Lista();
        for(int i=20000; i>=1; i--){
            int numeroAleatorio = (int) (Math.random()*20000+1);
            lista8.insertarFinal(numeroAleatorio);
        }
        long horaI8 = System.nanoTime();
        lista8.ordenarInsercion_Por_Nodos();
        long horaF8 = System.nanoTime()-horaI8;
        //System.out.println( lista8.toString());
        System.out.println("Método por Nodos para 20000 elementos aleatorios");
        System.out.println(horaF8 + " Nanosegundos");
        
        Lista<Integer> lista9 = new Lista();
        for(int i=200000; i>=1; i--){
            int numeroAleatorio = (int) (Math.random()*200000+1);
            lista9.insertarFinal(numeroAleatorio);
        }
        long horaI9 = System.nanoTime();
        lista9.ordenarInsercion_Por_Infos();
        long horaF9 = System.nanoTime()-horaI9;
        //System.out.println( lista9.toString());
        System.out.println("Método por Infos para 200000 elementos aleatorios");
        System.out.println(horaF9 + " Nanosegundos");
        
        Lista<Integer> lista10 = new Lista();
        for(int i=200000; i>=1; i--){
            int numeroAleatorio = (int) (Math.random()*200000+1);
            lista10.insertarFinal(numeroAleatorio);
        }
        long horaI10 = System.nanoTime();
        lista10.ordenarInsercion_Por_Nodos();
        long horaF10 = System.nanoTime()-horaI10;
        //System.out.println( lista10.toString());
        System.out.println("Método por Nodos para 200000 elementos aleatorios");
        System.out.println(horaF10 + " Nanosegundos");
        
        Lista<Integer> lista11 = new Lista();
        for(int i=5000000; i>=1; i--){
            int numeroAleatorio = (int) (Math.random()*5000000+1);
            lista11.insertarFinal(numeroAleatorio);
        }
        long horaI11 = System.nanoTime();
        lista11.ordenarInsercion_Por_Infos();
        long horaF11 = System.nanoTime()-horaI11;
        //System.out.println( lista11.toString());
        System.out.println("Método por Infos para 5000000 elementos aleatorios");
        System.out.println(horaF11 + " Nanosegundos");
        */
        Lista<Integer> lista12 = new Lista();
        for(int i=5000000; i>=1; i--){
            int numeroAleatorio = (int) (Math.random()*5000000+1);
            lista12.insertarFinal(numeroAleatorio);
        }
        long horaI12 = System.nanoTime();
        lista12.ordenarInsercion_Por_Nodos();
        long horaF12 = System.nanoTime()-horaI12;
        //System.out.println( lista12.toString());
        System.out.println("Método por Nodos para 5000000 elementos aleatorios");
        System.out.println(horaF12 + " Nanosegundos");
    }
}
