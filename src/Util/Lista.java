/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.util.Iterator;

/**
 *
 * @author RYZEN
 * @param <T>
 */
public class Lista <T> implements Iterable<T> {
    private Nodo<T> cab=null;
    private int tam=0;
    
    public Lista(){
        
    }
    
    public void insertarInicio(T info){
        this.cab = new Nodo<>(info, this.cab);
        this.tam++;
    }
    
    public void insertarFinal(T info){
        if(estaVacio()){
            insertarInicio(info);
        }
        else{
            Nodo<T> ultimo;
            try{
                ultimo = getNodoPos(tam-1);
                ultimo.setSiguiente(new Nodo(info, null));
                this.tam++;
            } 
            catch (Exception ex){
                System.err.println("Error inesperado");
            }
        } 
    }
    public T get(int pos){
        Nodo<T> n;
        try {
            n = getNodoPos(pos);
            return n.getInfo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public void set(int pos, T nuevo){
        try {
            Nodo<T> n = getNodoPos(pos);
            n.setInfo(nuevo);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public T eliminar(int pos){
        if(estaVacio())
            return null;
        Nodo<T> borrar = null;
        if(pos==0){
            borrar = this.cab;
            this.cab = this.cab.getSiguiente();
            borrar.setSiguiente(null);
        }
        else{
            try {
                Nodo<T> antes = getNodoPos(pos-1);
                borrar = antes.getSiguiente();
                antes.setSiguiente(borrar.getSiguiente());
                borrar.setSiguiente(null);
                
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                return null;
            }
            
        }
        this.tam--;
        return borrar.getInfo();
    }
    
    public boolean contieneRepetidos(){
        if(tam>1){
            for(Nodo<T> n = this.cab; n!=null; n=n.getSiguiente()){
                for(Nodo<T> m = n.getSiguiente(); m!=null; m=m.getSiguiente()){
                 if(n.getInfo() == m.getInfo())                   
                       return true;
                }
            }
        }
        return false;
    }
    
    public void eliminarRepetidos(){
        int aux=0;
        for(Nodo<T> n = this.cab; n!=null; n=n.getSiguiente()){
            int it = aux;
            for(Nodo<T> m = n.getSiguiente(); m!=null; m=m.getSiguiente()){
                it++;
                if(n.getInfo() == m.getInfo())                   
                    eliminar(it);
            }
            aux++;
        } 
    }
    
    public boolean equals(Lista<T> l){
        if(l.tam != this.tam)
            return false;
        Nodo<T> m = l.cab;
        for(Nodo<T> n = this.cab; n!=null; n=n.getSiguiente()){
            if(n.getInfo() != m.getInfo())
                return false;
            m=m.getSiguiente();
        }
        return true;
    }
    
    public Lista<T> unir(Lista<T> l){
        Lista<T> aux = new Lista();
        for(Nodo<T> n = this.cab; n!=null; n=n.getSiguiente()){
            aux.insertarFinal(n.getInfo());
        }
        
        for(Nodo<T> n = l.cab; n!=null; n=n.getSiguiente()){
            aux.insertarFinal(n.getInfo());
        }
        return aux;
    }
    
    public Lista<T> unirOrdenado(Lista<T> l){
        Lista<T> aux = new Lista();
        aux = unir(l);
        aux.sort();
        return aux;
    }
    public void ordenarInsercion_Por_Infos(){
        if(this.cab!=null && this.cab.getSiguiente()!=null){
         int cont = 0;
            for(int i=0; i<=cont;i++){
            Nodo<T> m = this.cab;
            Nodo<T> n = this.cab.getSiguiente();
            T aux =  m.getInfo();
            try {
                
                while(n!=null ){
                    if(comparador(m.getInfo(),n.getInfo())>0){
                    intercambio(m,n);
                    }
                    m=n;
                    n=n.getSiguiente();
                    cont++;
                }
                
            }
            catch (Exception ex) {
                    System.err.println(ex.getMessage());
                } 
            
            }
        }
        
    }
    
    private void intercambio(Nodo<T> men, Nodo<T> actual)
    {
        T info=men.getInfo();
        men.setInfo(actual.getInfo());
        actual.setInfo(info);
    }
    
    private int comparador(T info1, T info2)
    {
    Comparable c=(Comparable)info1;
    return c.compareTo(info2);
    }
    
    public void ordenarInsercion_Por_Nodos(){
        if(this.cab!=null && this.cab.getSiguiente()!=null){
            Nodo<T> actual = this.cab;
            Nodo<T> aux = null;
        
            while(actual!=null){
                Nodo<T> siguiente = actual.getSiguiente();
                if(aux==null || comparador(aux.getInfo(), actual.getInfo())>0){
                    actual.setSiguiente(aux);
                    aux = actual;
                }
                else {
                    Nodo<T> ordenar = aux;
                    while(ordenar.getSiguiente()!=null && comparador(ordenar.getSiguiente().getInfo(), actual.getInfo()) <0)
                    {
                        ordenar=ordenar.getSiguiente();
                    }
                    actual.setSiguiente(ordenar.getSiguiente());
                    ordenar.setSiguiente(actual);
                }
                actual = siguiente;
            }
            this.cab = aux;
        }
    }
    
    public void sort(){
        for (Nodo<T> n = this.cab; n!=null; n=n.getSiguiente()){
            for (Nodo<T> m = this.cab; m!=null && m.getSiguiente()!=null; m=m.getSiguiente()){
                if((Integer)m.getSiguiente().getInfo() < (Integer)m.getInfo()){
                    T aux=m.getSiguiente().getInfo();
                    m.getSiguiente().setInfo(m.getInfo());
                    m.setInfo(aux);
                }
            }
        }
    }
    
    public boolean estaVacio(){
        if(this.tam==0)return true;
        return false;
    }
    
    private Nodo<T> getNodoPos(int pos) throws Exception{
        if(estaVacio() || pos<0 || pos>tam){
            throw new Exception("La posicion solicitada no es valida");
        }
        else{
            Nodo<T> n=this.cab;
            while(pos>0){
                n=n.getSiguiente();
                pos--;
            }
            return n;
        }  
    }
    
    @Override
    public String toString(){
        String msg="";
        for(Nodo<T> n=this.cab; n!=null; n=n.getSiguiente()){
            msg+=n.getInfo().toString()+" ";
        }
        return msg;      
    }
    
    public int getTamaño(){
        return this.tam;
    }
    @Override
    public Iterator<T> iterator() {        
        return (Iterator<T>) new IteratorLS<T>(this.cab) {};        
    }
    public Object[] aVector(){
         if(this.estaVacio())
                return (null);
        Object vector[]=new Object[this.tam];
        Iterator<T> it=this.iterator();
        int i=0;
        while(it.hasNext())
            vector[i++]=it.next();
        return(vector);
    }
    public T remove(int i) {        
        if(this.estaVacio())
            return null;        
        Nodo<T> t=this.cab;        
        if(i==0)
            this.cab=this.cab.getSiguiente();
        else{            
            try {                
                Nodo<T> y=this.getNodoPos(i-1);
                t=y.getSiguiente();
                y.setSiguiente(t.getSiguiente());                
            }catch(Exception e){                
                    System.err.println(e.getMessage());
                    return (null);
            }            
        }        
        t.setSiguiente(null);        
        this.tam--;        
        return(t.getInfo());        
    }

    private static class IteratorLS<T> {

        public IteratorLS(Nodo<T> cab) {
        }
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Lista<?> other = (Lista<?>) obj;
        
        Nodo<?> cab1=this.cab;
        Nodo<?> cab2=other.cab;
        while(cab1!=null && cab2!=null)
        {
            if(!cab1.getInfo().equals(cab2.getInfo()))
                return false;
            cab1=cab1.getSiguiente();
            cab2=cab2.getSiguiente();
        }
        return cab1==null && cab2==null;
    }
}
