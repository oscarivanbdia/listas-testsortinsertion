/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 * @author MADARME
 */
public class Conjunto<T> {
    //Estructura de datos estática
    private Caja<T> []cajas;
    private int i=0;
    
    public Conjunto(){}
    
    public Conjunto(int cantidadCajas)
    {
    if(cantidadCajas <=0)
           throw new RuntimeException("No se pueden crear el Conjunto");
        
     this.cajas=new Caja[cantidadCajas];
    }
    
    
    public void adicionarElemento(T nuevo) throws Exception
    {
        if(i>=this.cajas.length)
            throw new Exception("No hay espacio en el Conjunto");
        
        if(this.existeElemento(nuevo))
            throw new Exception("No se puede realizar inserción, elemento repetido");
        
        
        this.cajas[i]=new Caja(nuevo);
        this.i++;
    
    }
    
    public T get(int indice)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        return this.cajas[indice].getObjeto();
            
    }
    
    
    public int indexOf(T objBuscar)
    {
    
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(objBuscar))
                return j;
        }
        
        return -1;
        
    }
    
    public void set(int indice, T nuevo)
    {
        if(indice <0 || indice>=this.getLength())
            throw new RuntimeException("Índice fuera de rango");
        
        this.cajas[indice].setObjeto(nuevo);
            
    }
    
    
    public boolean existeElemento(T nuevo)
    {
        
        //Sólo estoy comparando por los estudiantes matriculados
        for(int j=0;j<i;j++)
        {
            
            //Sacando el estudiante de la caja:
            T x= this.cajas[j].getObjeto();
            
            if(x.equals(nuevo))
                return true;
        }
        
        return false;
    
    }
    
    
    /**
     *  para el grupo A--> Selección
     *  para el grupo C--> Inserción
     * 
     */
    public void ordenar()
    {
        int p, j;
        int aux;
        for (p = 1; p < i; p++){ // desde el segundo elemento hasta
              aux = (int) cajas[p].getObjeto(); // el final, guardamos el elemento y
              j = p - 1; // empezamos a comprobar con el anterior
              while ((j >= 0) && (aux < (int)cajas[j].getObjeto())){ // mientras queden posiciones y el
                                                                    // valor de aux sea menor que los
                             cajas[j + 1] = cajas[j];       // de la izquierda, se desplaza a
                             j--;                   // la derecha
              }
              Caja nueva = new Caja(aux);
              this.cajas[j + 1] = nueva; // colocamos aux en su sitio
    }
    
    }
    
    
    /**
     * Realiza el ordenamiento por burbuja 
     */
    public void ordenarBurbuja()
    {
        Caja aux;
        for(int j=0; j<i-1 ; j++){
            for(int k=0 ; k<i-1 ; k++){
                if( (int)this.cajas[k+1].getObjeto() < (int)this.cajas[k].getObjeto() ){
                    aux=this.cajas[k+1];
                    this.cajas[k+1]=this.cajas[k];
                    this.cajas[k]=aux;
                }
            }
        }
    }
    
    /**
     * Elimina un elemento del conjunto y compacta
     * @param objBorrado es el objeto que deseo eliminar
     * @throws java.lang.Exception
     */
    
    public void remover(T objBorrado) throws Exception
    {
        
        int p = indexOf(objBorrado);
        if (p==-1) 
            throw new Exception("No se puede remover, el elemento no existe");
        int j;
        
        
        for ( j = p; j < i - 1; j++) {
            cajas[j] = cajas[j+1];
        }
        cajas[i - 1] = null;
        this.i--;
    }
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío. En este proceso no se toma en cuenta los datos repetidos
     * Ejemplo:
     *  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2)
     *  da como resultado: conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenar(Conjunto<T> nuevo)
    {
        int x = nuevo.getLength() + this.getLength();
        Caja<T> []aux = new Caja[x];
        for(int a = 0; a< this.getCapacidad() ; a++){
            aux[a] = this.cajas[a];
        }
        for(int b = 0; b<nuevo.getCapacidad(); b++){
            aux[i] = nuevo.cajas[b];
            this.i++;
        }
        this.cajas=aux;
        nuevo.removeAll();
    }
    
    
    /**
     *  El método adiciona todos los elementos de nuevo en el conjunto original(this) y 
     * el nuevo queda vacío.En este proceso SI toma en cuenta los datos repetidos
 Ejemplo:
  conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
 conjunto1.concatenar(conjunto2)
  da como resultado: conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     * @throws java.lang.Exception
     */
    public void concatenarRestrictivo(Conjunto<T> nuevo) throws Exception
    {
        for (int j=0; j<this.getCapacidad(); j++){
            for (int k=0; k<nuevo.getCapacidad() ; k++){
                if(nuevo.cajas[k].getObjeto().equals(this.cajas[j].getObjeto()))
                    nuevo.remover(nuevo.cajas[k].getObjeto());
            }
        }
        this.concatenar(nuevo);
    }
    
    public Conjunto<T> getDiferencia(Conjunto<T> c2) throws Exception
    {	
	Conjunto<T> c3 = new Conjunto(this.getLength());

	for(int j=0;j<this.getCapacidad();j++)
	{
		if( !c2.existeElemento(this.get(j)) )
			c3.adicionarElemento(this.get(j));
	}
    return c3;
    }   

    
    public void removeAll()
    {
        this.cajas=null;
        this.i=0;
    }
    
    
    @Override
    public String toString() {
        String msg="******** CONJUNTO*********\n";
        
        for(int j=0;j<this.getCapacidad();j++)
            msg+=cajas[j].getObjeto().toString()+"\n";
        
        return msg;
    }
    
    
    
    /**
     * Obtiene la cantidad de elementos almacenados
     * @return  retorna un entero con la cantidad de elementos
     */
    public int getCapacidad()
    {
        return this.i;
    }
    
    /**
     *  Obtiene el tamaño máximo de cajas dentro del Conjunto
     * @return int con la cantidad de cajas
     */
    public int getLength()
    {
        return this.cajas.length;
    }
    
    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee elementos repetidos.
     * @return el elemnto mayor de la colección
     */
    public T getMayorElemento()
    {
    if(this.cajas==null)
        throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
    
    T mayor=this.cajas[0].getObjeto();
    for(int i=1;i<this.getCapacidad();i++)
    {
        //Utilizo la interfaz comparable y después su método compareTo
        Comparable comparador=(Comparable)mayor;
        T dato_A_comparar=this.cajas[i].getObjeto();
        // Resta entra mayor-datoAComparar 
        int rta_compareTo=comparador.compareTo(dato_A_comparar);
        if(rta_compareTo<0)
            mayor=dato_A_comparar;
    }
    return mayor;
    }
}
