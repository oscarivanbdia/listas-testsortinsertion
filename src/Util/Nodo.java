/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author RYZEN
 */
public class Nodo <T> {
     private T info;
    private Nodo<T> sig;
    
    Nodo(){
        
    }
    
    Nodo(T info, Nodo<T> sig){
        this.info = info;
        this.sig = sig;
    }
    
    T getInfo(){
        return this.info;
    }
    Nodo<T> getSiguiente(){
        return this.sig;
    }
    
    void setInfo(T newInfo){
        this.info = newInfo;
    }
    
    void setSiguiente(Nodo<T> sig){
        this.sig = sig;
    }
}
