/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Harold
 */
public class ListaTest {
    
    public ListaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insertarInicio method, of class Lista.
     */
    @Test
    public void testEliminar() {
        System.out.println("eliminar");
        int pos = 0;
        Lista<Integer> instance = new Lista();
        instance.insertarFinal(7);
        instance.insertarFinal(4);
        Object expResult = 4;
        instance.eliminar(pos);
        Object result = instance.get(0);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.        
    }

    /**
     * Test of equals method, of class Lista.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Lista<Integer> instance = new Lista();
        instance.insertarInicio(1);
        boolean expResult = true;
        Lista<Integer> instance2 = new Lista();
        instance2.insertarInicio(1);
        boolean result = instance.equals(instance2);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of ordenarInsercion_Por_Nodos method, of class Lista.
     */
    @Test
    public void testOrdenarInsercion_Por_Nodos() {
        System.out.println("ordenarInsercion_Por_Nodos");
        Lista<Integer> instance = new Lista();
        instance.insertarFinal(5);
        instance.insertarFinal(7);
        instance.insertarFinal(2);
        instance.ordenarInsercion_Por_Nodos();
        Lista<Integer> instance2 = new Lista();
        instance2.insertarFinal(2);
        instance2.insertarFinal(5);
        instance2.insertarFinal(7);
        assertEquals(instance, instance2);
        // TODO review the generated test code and remove the default call to fail.
        
        
    }
    
}
